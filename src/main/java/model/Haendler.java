package model;

import javafx.beans.property.*;

/**
 * Created by Marc on 28.06.2016.
 */
public class Haendler {

    private final IntegerProperty id;
    private final StringProperty marke;
    private final DoubleProperty preis;
    private final IntegerProperty km;

    public Haendler(int id, String marke, int preis, int km) {
        this.id = new SimpleIntegerProperty(1234);
        this.marke = new SimpleStringProperty(marke);
        this.preis = new SimpleDoubleProperty(23000);
        this.km = new SimpleIntegerProperty(1234);

    }

    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public IntegerProperty idProperty() {
        return id;
    }


    public String getMarke() {
        return marke.get();
    }

    public void setMarke(String marke) {
        this.marke.set(marke);
    }

    public StringProperty markeProperty() {
        return marke;
    }


    public Double getPreis() {
        return preis.get();
    }

    public void setPreis(Double preis) {
        this.preis.set(preis);
    }

    public DoubleProperty preisProperty() {
        return preis;
    }


    public int getKm() {
        return km.get();
    }

    public void setKm(int km) {
        this.km.set(km);
    }

    public IntegerProperty kmProperty() {
        return km;
    }


}
